/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise1;

import javax.ejb.Stateless;
import javax.inject.Named;

/**
 *
 * @author ckoul
 */
@Named
@Stateless
public class MessageServerBean {
    
    public String getMessage() {
        return "Hello World!";
    }
}
