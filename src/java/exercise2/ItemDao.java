/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise2;

import java.util.List;

/**
 *
 * @author ckoul
 */
public interface ItemDao {
    
    List<Item> fetchItems();
}
