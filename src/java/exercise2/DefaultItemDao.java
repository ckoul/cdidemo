/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exercise2;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ckoul
 */
public class DefaultItemDao implements ItemDao {
    
    @Override
    public List<Item> fetchItems() {
        List<Item> results = new ArrayList<>();
        results.add(new Item(34, 7));
        results.add(new Item(4, 37));
        results.add(new Item(24, 19));
        results.add(new Item(89, 32));
        return results;
    }
}
